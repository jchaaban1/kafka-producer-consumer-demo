FROM openjdk:20-jdk

WORKDIR /app

COPY target/*.jar /app/product_service.jar

EXPOSE 9000

# Run the JAR file when the container launches
CMD ["java", "-jar", "product_service.jar"]
