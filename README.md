# Kafka Producer-Consumer Demo

## Overview

This project showcases a microservice architecture using Apache Kafka to process product creation events.
It demonstrates the configuration of Kafka within Spring Boot using `application.yaml` and Java code,
highlighting the integration of Kafka for robust message handling between services. 

The system includes two Spring Boot microservices: `product-service` and `inventory-service`, each backed by its own 
PostgreSQL database. This setup uses Testcontainers for integration and unit/slice testing to ensure each component 
interacts accurately with Kafka and the databases under test conditions. 

Additionally, the demo includes a detailed Kubernetes deployment that orchestrates multiple Kafka instances, 
the two microservices, and the separate databases, showcasing a scalable and isolated environment for each component.


## Architecture

- **Product Service**: Manages product creation and publishes events to a Kafka topic.
- **Inventory Service**: Consumes product creation events from Kafka and updates the inventory accordingly.

## Technologies

- **Java**
- **Spring Boot**
- **Apache Kafka**
- **Docker and docker-compose (for local development)**:
- **Kubernetes**

## Setup and Installation

### Prerequisites

- Java JDK 17
- Maven
- Docker
- Kubernetes (Minikube or similar for local deployment)
- Kafka (Setup within Kubernetes or standalone)

### Building the Application

1. Clone the repository:
   ```bash
   git clone git@gitlab.com:jchaaban1/kafka-producer-consumer-demo.git 
   cd kafka-producer-consumer-demo
