package com.jchaaban.kafkaproductsmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaProductsMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaProductsMicroserviceApplication.class, args);
    }

}
