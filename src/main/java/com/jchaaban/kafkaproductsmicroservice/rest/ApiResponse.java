package com.jchaaban.kafkaproductsmicroservice.rest;

public class ApiResponse {
    private int code;
    private final String message;

    public ApiResponse(String message, int errorCode) {
        this.code = errorCode;
        this.message = message;
    }

    public ApiResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
