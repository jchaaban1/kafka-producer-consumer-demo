package com.jchaaban.kafkaproductsmicroservice.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateProductDto {
    @JsonProperty
    private String title;
    @JsonProperty
    private double price;
    @JsonProperty
    private int quantity;

    public CreateProductDto(String title, double price, int quantity) {
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public CreateProductDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
