package com.jchaaban.kafkaproductsmicroservice.rest.exception;

public class ProductCreationException extends RuntimeException {

    public ProductCreationException(String message) {
        super(message);
    }
}
