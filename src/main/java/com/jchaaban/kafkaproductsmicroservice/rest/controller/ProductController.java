package com.jchaaban.kafkaproductsmicroservice.rest.controller;

import com.jchaaban.kafkaproductsmicroservice.rest.ApiResponse;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.CreateProductDto;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.ReadProductDto;
import com.jchaaban.kafkaproductsmicroservice.rest.exception.ProductCreationException;
import com.jchaaban.kafkaproductsmicroservice.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    public static final int PRODUCT_CREATION_EXCEPTION = 4;


    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<?> createProduct(@RequestBody CreateProductDto createProductDto) {
        try {
            ReadProductDto readProductDto = productService.createProductAsync(createProductDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(readProductDto);
        } catch (Exception exception) {
            logger.error("Error creating product: {}", exception.getMessage(), exception);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error creating product");
        }
    }

    @ExceptionHandler(ProductCreationException.class)
    public ResponseEntity<ApiResponse> handleRoomNotFoundException(ProductCreationException productCreationException) {
        ApiResponse apiResponse = new ApiResponse(productCreationException.getMessage(), PRODUCT_CREATION_EXCEPTION);
        return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
    }

}
