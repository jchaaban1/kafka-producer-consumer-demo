package com.jchaaban.kafkaproductsmicroservice.service;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.kafkaproductsmicroservice.repository.ProductEntity;
import com.jchaaban.kafkaproductsmicroservice.repository.ProductRepository;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.CreateProductDto;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.ReadProductDto;
import com.jchaaban.kafkaproductsmicroservice.rest.exception.ProductCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductService.class);

    private final KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate;
    private final ProductRepository productRepository;


    public ProductService(KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate, ProductRepository productRepository) {
        this.kafkaTemplate = kafkaTemplate;
        this.productRepository = productRepository;
    }

    public ReadProductDto createProductAsync(CreateProductDto createProductDto) {
        String productId = UUID.randomUUID().toString();

        ProductCreatedEvent productCreatedEvent = createProductEvent(productId, createProductDto);

        sendProductEventAsync(productId, productCreatedEvent);

        saveProduct(productCreatedEvent);

        return new ReadProductDto(
                productCreatedEvent.getProductId(),
                productCreatedEvent.getTitle(),
                productCreatedEvent.getPrice(),
                productCreatedEvent.getQuantity()
        );
    }

    private ProductCreatedEvent createProductEvent(String productId, CreateProductDto createProductDto) {
        return new ProductCreatedEvent(
                productId,
                createProductDto.getTitle(),
                createProductDto.getQuantity(),
                createProductDto.getPrice()
        );
    }

    private void sendProductEventAsync(String productId, ProductCreatedEvent productCreatedEvent) {
        CompletableFuture<SendResult<String, ProductCreatedEvent>> future = kafkaTemplate.send(
                "product-created-events-topic",
                productId,
                productCreatedEvent
        );

        future.whenComplete(this::handleSendResult);

        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ProductCreationException("An error occurred while sending ProductCreatedEvent");
        }
    }

    private void saveProduct(ProductCreatedEvent productCreatedEvent) {
        productRepository.save(
                new ProductEntity(
                        productCreatedEvent.getProductId(),
                        productCreatedEvent.getTitle(),
                        productCreatedEvent.getQuantity(),
                        productCreatedEvent.getPrice()
                ));
    }

    private void handleSendResult(SendResult<String, ProductCreatedEvent> result, Throwable exception) {
        if (exception != null) {
            logger.error("Failed to send ProductCreatedEvent: {}", exception.getMessage(), exception);
        } else {
            logResult(result);
        }
    }

    private void logResult(SendResult<String, ProductCreatedEvent> result) {
        logger.info("Topic: {}", result.getRecordMetadata().topic());
        logger.info("Offset: {}", result.getRecordMetadata().offset());
        logger.info("Partition: {}", result.getRecordMetadata().partition());
    }
}
