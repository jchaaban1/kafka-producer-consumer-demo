package testing.helper;

import com.jchaaban.common.ProductCreatedEvent;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class KafkaTester {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private ProductCreatedEvent receivedMessage;


    public ProductCreatedEvent getKafkaMessage() throws InterruptedException {
        while (receivedMessage == null){
            logger.info("waiting");
            Thread.sleep(500);
        }
        return receivedMessage;
    }

    @KafkaListener(topics = "product-created-events-topic", groupId = "test-group")
    public void listen(ProductCreatedEvent message) {
        receivedMessage = message;
    }
}
