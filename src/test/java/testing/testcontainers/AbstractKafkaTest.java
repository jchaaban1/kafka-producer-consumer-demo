package testing.testcontainers;

import com.jchaaban.kafkaproductsmicroservice.repository.ProductRepository;
import com.jchaaban.kafkaproductsmicroservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import testing.helper.KafkaTester;

@ActiveProfiles(value = "test")
@ContextConfiguration(initializers = {KafkaContextInitializer.class})
@SpringBootTest(
        classes = {
                KafkaTestContainersConfiguration.class,
                ProductService.class,
                KafkaTester.class,
        }
)
@ImportAutoConfiguration(classes = {KafkaAutoConfiguration.class})
@DirtiesContext
public class AbstractKafkaTest {

    @Autowired
    protected KafkaTester kafkaTester;

    @MockBean
    protected ProductRepository productRepository;
}
