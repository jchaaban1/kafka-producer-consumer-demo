package com.jchaaban.kafkaproductsmicroservice.unit;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.CreateProductDto;
import com.jchaaban.kafkaproductsmicroservice.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import testing.testcontainers.AbstractKafkaTest;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductServiceTest extends AbstractKafkaTest {

    @Autowired
    private ProductService productService;

    @Test
    public void createProductSuccessfully() throws InterruptedException {

        String title = "title";
        int quantity = 3;
        double price = 3;

        CreateProductDto createProductDto = new CreateProductDto(title, price, quantity);
        productService.createProductAsync(createProductDto);

        ProductCreatedEvent receivedMessage = kafkaTester.getKafkaMessage();

        assertThat(receivedMessage.getTitle()).isEqualTo(title);
        assertThat(receivedMessage.getPrice()).isEqualTo(price);
        assertThat(receivedMessage.getQuantity()).isEqualTo(quantity);
    }
}
