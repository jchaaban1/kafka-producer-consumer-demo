package com.jchaaban.kafkaproductsmicroservice.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.kafkaproductsmicroservice.repository.ProductEntity;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.CreateProductDto;
import com.jchaaban.kafkaproductsmicroservice.rest.dto.ReadProductDto;
import org.assertj.core.api.AssertionsForClassTypes;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ProductServiceAppIT extends AbstractIntegrationTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void shouldCreateProduct() throws Exception {
        String title = "Test Product";
        double price = 100.0;
        int quantity = 3;
        CreateProductDto createProductDto = new CreateProductDto(title, price, quantity);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createProductDto)))
                .andExpect(status().isCreated())
                .andReturn();

        ReadProductDto readProductDto =
                objectMapper.readValue(result.getResponse().getContentAsString(), ReadProductDto.class);

        ProductCreatedEvent receivedMessage = kafkaTester.getKafkaMessage();

        assertThat(readProductDto.getTitle()).isEqualTo(createProductDto.getTitle());
        assertThat(readProductDto.getPrice()).isEqualTo(createProductDto.getPrice());
        assertThat(readProductDto.getQuantity()).isEqualTo(createProductDto.getQuantity());

        assertThat(receivedMessage.getTitle()).isEqualTo(createProductDto.getTitle());
        assertThat(receivedMessage.getPrice()).isEqualTo(createProductDto.getPrice());
        assertThat(receivedMessage.getQuantity()).isEqualTo(createProductDto.getQuantity());

        List<ProductEntity> allProducts = productRepository.findAll();
        AssertionsForInterfaceTypes.assertThat(allProducts).hasSize(1);

        ProductEntity firstProduct = allProducts.get(0);
        AssertionsForClassTypes.assertThat(firstProduct.getTitle()).isEqualTo(title);
        AssertionsForClassTypes.assertThat(firstProduct.getQuantity()).isEqualTo(quantity);
        AssertionsForClassTypes.assertThat(firstProduct.getPrice()).isEqualTo(price);
    }
}
