package com.jchaaban.kafkaproductsmicroservice.it;

import com.jchaaban.kafkaproductsmicroservice.KafkaProductsMicroserviceApplication;
import com.jchaaban.kafkaproductsmicroservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import testing.helper.KafkaTester;
import testing.testcontainers.KafkaContextInitializer;
import testing.testcontainers.PostgresContextInitializer;

@ActiveProfiles("test")
@ContextConfiguration(initializers = {KafkaContextInitializer.class, PostgresContextInitializer.class})
@SpringBootTest(classes = KafkaProductsMicroserviceApplication.class)
@AutoConfigureMockMvc
@Import(KafkaTester.class)
@DirtiesContext
public class AbstractIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected KafkaTester kafkaTester;

    @Autowired
    protected ProductRepository productRepository;
}
