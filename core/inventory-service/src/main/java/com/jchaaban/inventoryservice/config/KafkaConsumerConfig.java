package com.jchaaban.inventoryservice.config;

import com.jchaaban.inventoryservice.exception.NotRetryableException;
import com.jchaaban.inventoryservice.exception.RetryableException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.util.backoff.FixedBackOff;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
@Profile("!test")
public class KafkaConsumerConfig {

    public static final int RETRY_WAIT_TIME = 2000;
    public static final int MAX_RETRY_ATTEMPTS = 3;

    final Environment environment;

    public KafkaConsumerConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    KafkaTemplate<String, Object> kafkaTemplate(ProducerFactory<String, Object> producerFactory){
        return new KafkaTemplate<>(producerFactory);
    }

    @Bean
    ProducerFactory<String, Object> producerFactory(){
        HashMap<String, Object> config = new HashMap<>();

        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, environment.getProperty("kafka.bootstrap-servers"));
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        return new DefaultKafkaProducerFactory<>(config);
    }

    @Bean
    ConsumerFactory<String, Object> consumerFactory() {
        Map<String, Object> config = new HashMap<>();

        // Set the bootstrap servers for this Kafka consumer
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, environment.getProperty("kafka.bootstrap-servers"));

        // Set the deserializer class for message keys (in this case, strings)
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        // Specify the deserializer class for JSON message values
        config.put(ErrorHandlingDeserializer.VALUE_DESERIALIZER_CLASS, JsonDeserializer.class);

        // When a message is successfully deserialized without any errors, ErrorHandlingDeserializer delegates
        // the deserialization task to the actual deserializer class specified for the value
        // (in this case, JsonDeserializer.class). The deserialized object is then returned If an error occurs during
        // deserialization, ErrorHandlingDeserializer catches the exception and handles it gracefully.
        // Instead of throwing the exception and potentially crashing the consumer, it returns a special error object
        // (DeserializationException), which contains information about the error (such as the original exception and
        // the raw bytes of the message). This allows the consumer to handle the error appropriately without crashing,
        // for example, by logging the error or processing the message differently.
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ErrorHandlingDeserializer.class);

        // Set the trusted packages for JSON deserialization to prevent security vulnerabilities
        config.put(JsonDeserializer.TRUSTED_PACKAGES, environment.getProperty("consumer.json.trusted.packages"));

        // Set the group ID for the Kafka consumer, obtained from the application's environment properties
        config.put(ConsumerConfig.GROUP_ID_CONFIG, environment.getProperty("spring.kafka.consumer.group-id"));


        return new DefaultKafkaConsumerFactory<>(config);
    }

    @Bean
    ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
            ConsumerFactory<String, Object> consumerFactory,
            KafkaTemplate<String, Object> kafkaTemplate
    ){
        DefaultErrorHandler errorHandler = new DefaultErrorHandler(
                new DeadLetterPublishingRecoverer(kafkaTemplate),
                new FixedBackOff(RETRY_WAIT_TIME, MAX_RETRY_ATTEMPTS)
                // wait for RETRY_WAIT_TIME seconds before trying to consume the message again
                // send the message to a dead letter topic if the message consumption was not successful after
                // MAX_RETRY_ATTEMPTS retries
        );

        errorHandler.addNotRetryableExceptions(NotRetryableException.class);
        errorHandler.addRetryableExceptions(RetryableException.class);

        ConcurrentKafkaListenerContainerFactory<String, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        factory.setCommonErrorHandler(errorHandler);
        return factory;
    }

}
