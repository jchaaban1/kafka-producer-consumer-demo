package com.jchaaban.inventoryservice.eventhandler;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.inventoryservice.repository.ProductEntity;
import com.jchaaban.inventoryservice.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@KafkaListener(topics = "product-created-events-topic")
public class ProductCreatedEventHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ProductCreatedEvent productCreatedEvent = null;

    private final ProductRepository productRepository;

    public ProductCreatedEventHandler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @KafkaHandler
    public void handle(ProductCreatedEvent productCreatedEvent) {
        this.productCreatedEvent = productCreatedEvent;
        logger.info("Received a new event: " + productCreatedEvent);
        saveProduct(productCreatedEvent);
    }

    private void saveProduct(ProductCreatedEvent productCreatedEvent) {
        productRepository.save(
                new ProductEntity(
                        productCreatedEvent.getProductId(),
                        productCreatedEvent.getTitle(),
                        productCreatedEvent.getQuantity(),
                        productCreatedEvent.getPrice()
                ));
    }

    public ProductCreatedEvent getProductCreatedEvent() {
        return productCreatedEvent;
    }
}
