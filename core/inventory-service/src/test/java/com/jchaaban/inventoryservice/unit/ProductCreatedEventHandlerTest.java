package com.jchaaban.inventoryservice.unit;

import com.jchaaban.common.ProductCreatedEvent;
import org.junit.jupiter.api.Test;
import testing.testcontainers.AbstractKafkaTest;

import java.util.UUID;
import java.util.logging.Logger;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ProductCreatedEventHandlerTest extends AbstractKafkaTest {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Test
    void consumeProductCreatedEventSuccessfully() throws InterruptedException {
        String productId = UUID.randomUUID().toString();

        ProductCreatedEvent sentProductCreatedEvent = new ProductCreatedEvent(
                productId,
                "product-title",
                3,
                3.0
        );
        kafkaTemplate.send(
                "product-created-events-topic",
                UUID.randomUUID().toString(),
                sentProductCreatedEvent
        );

        ProductCreatedEvent receivedProductCreatedEvent = productCreatedEventHandler.getProductCreatedEvent();

        while (receivedProductCreatedEvent == null){
            logger.info("waiting");
            receivedProductCreatedEvent = productCreatedEventHandler.getProductCreatedEvent();
            Thread.sleep(500);
        }

        assertThat(receivedProductCreatedEvent.getProductId()).isEqualTo(sentProductCreatedEvent.getProductId());
        assertThat(receivedProductCreatedEvent.getTitle()).isEqualTo(sentProductCreatedEvent.getTitle());
        assertThat(receivedProductCreatedEvent.getQuantity()).isEqualTo(sentProductCreatedEvent.getQuantity());
        assertThat(receivedProductCreatedEvent.getPrice()).isEqualTo(sentProductCreatedEvent.getPrice());
    }

}
