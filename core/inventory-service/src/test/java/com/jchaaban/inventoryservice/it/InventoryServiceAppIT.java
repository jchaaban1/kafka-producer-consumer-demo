package com.jchaaban.inventoryservice.it;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.inventoryservice.repository.ProductEntity;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class InventoryServiceAppIT extends AbstractIntegrationTest {

    public static final int EVENT_CONSUMPTION_TIMEOUT = 2000;

    @Test
    void shouldCreateProduct(){
        String productId = UUID.randomUUID().toString();
        String title = "test-product";
        int quantity = 3;
        double price = 3.0;

        ProductCreatedEvent productCreatedEvent = createProductCreatedEvent(productId, title, quantity, price);

        sendProductCreatedEvent(productId, productCreatedEvent);

        waitForEventConsumption();

        List<ProductEntity> allProducts = productRepository.findAll();
        assertThat(allProducts).hasSize(1);

        ProductEntity firstProduct = allProducts.get(0);
        assertThat(firstProduct.getTitle()).isEqualTo(title);
        assertThat(firstProduct.getQuantity()).isEqualTo(quantity);
        assertThat(firstProduct.getPrice()).isEqualTo(price);
    }

    private void sendProductCreatedEvent(String productId, ProductCreatedEvent productCreatedEvent) {
        kafkaTemplate.send(
                "product-created-events-topic",
                productId,
                productCreatedEvent
        );
    }

    @NotNull
    private static ProductCreatedEvent createProductCreatedEvent(String productId, String title, int quantity, double price) {
        return new ProductCreatedEvent(
                productId,
                title,
                quantity,
                price
        );
    }

    private static void waitForEventConsumption() {
        try {
            Thread.sleep(EVENT_CONSUMPTION_TIMEOUT);
        } catch (Exception exception){
            System.out.println("An error occurred while waiting for the event to be consumed");
        }
    }
}
