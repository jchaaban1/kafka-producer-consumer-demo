package com.jchaaban.inventoryservice.it;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.inventoryservice.InventoryServiceApplication;
import com.jchaaban.inventoryservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import testing.testcontainers.KafkaContextInitializer;
import testing.testcontainers.PostgresContextInitializer;

@DirtiesContext
@ActiveProfiles(value = "test")
@ContextConfiguration(initializers = {KafkaContextInitializer.class, PostgresContextInitializer.class})
@SpringBootTest(classes = InventoryServiceApplication.class)
@AutoConfigureMockMvc
public class AbstractIntegrationTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate;

    @Autowired
    protected ProductRepository productRepository;
}
