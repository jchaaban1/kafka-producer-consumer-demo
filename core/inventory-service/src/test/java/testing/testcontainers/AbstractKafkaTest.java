package testing.testcontainers;

import com.jchaaban.common.ProductCreatedEvent;
import com.jchaaban.inventoryservice.eventhandler.ProductCreatedEventHandler;
import com.jchaaban.inventoryservice.repository.ProductRepository;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ActiveProfiles("test")
@ContextConfiguration(initializers = {KafkaContextInitializer.class})
@SpringBootTest(
        classes = {
                ProductCreatedEventHandler.class,
                KafkaTestContainersConfiguration.class,
        }
)

@ImportAutoConfiguration(classes = {KafkaAutoConfiguration.class})
@DirtiesContext
public class AbstractKafkaTest {

    @Autowired
    protected KafkaTemplate<String, ProductCreatedEvent> kafkaTemplate;

    @MockBean
    protected ProductRepository productRepository;

    @Autowired
    protected ProductCreatedEventHandler productCreatedEventHandler;
}
