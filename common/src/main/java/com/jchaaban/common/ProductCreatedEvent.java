package com.jchaaban.common;

public class ProductCreatedEvent {

    private String productId;
    private String title;
    private int quantity;
    private double price;

    public ProductCreatedEvent(String productId, String title, int quantity, double price) {
        this.productId = productId;
        this.title = title;
        this.quantity = quantity;
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public ProductCreatedEvent() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "id: " + productId + System.lineSeparator()
                + "title: " + title + System.lineSeparator()
                + "quantity: " + quantity + System.lineSeparator()
                + "price: " + price + System.lineSeparator();
    }
}
